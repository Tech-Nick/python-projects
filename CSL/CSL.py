#Python project to build the cheapest shopping list (food based) by scraping from the major food supermarkets.

import requests
import re
from bs4 import BeautifulSoup
import datetime
from decimal import Decimal
from operator import itemgetter
from Supermarket import Supermarket 

#intanstiating the supermarket object Tesco
Tesco = Supermarket("Tesco")

#helper functions
#Take in two numbers and calculate how many items are needed to reach the required qty
#i.e.  NeededQty / itemNumber
def howMany(qtyNeeded, qtyPerItem):
    units = qtyPerItem
    NumOfItems = 1
    if(qtyNeeded <= int(qtyPerItem)):
        return NumOfItems
    while units < qtyNeeded:
        units +=qtyPerItem
        NumOfItems+=1
    return NumOfItems

#calculat th number of items needed for teh desired qty
def total(numOfItems, price):       
    return Decimal(str(numOfItems)) * Decimal(price.strip('£'))

date = datetime.datetime.now()
dateNow = date.strftime("%d-%m-%Y %H:%M")

#Enter item to be searched.
List = []

print("Welcome to Cheapest Shopping List!")
print("Here you can build a shopping list and check " +Tesco.getName()+" for the cheapest price")
print("1) Enter a product to search " +Tesco.getName()+" for the cheapest one")
print("2) Enter the desired quantity if you have one or leave blank")
print("PLease enter a grocery item then press enter")
userInput = input()
List = userInput.split()
print("Now please enter the desired qty leave blank")
qtyNeeded = input()

#first item in the list
FirstItem = List[0]

#use above item to scrape supermarkets.

#setup the user agent for the browser you are using
headers = {"User-Agent": 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36 OPR/67.0.3575.137'}
if "eggs" in List:
   page = requests.get(Tesco.getURL("eggs"), headers=headers)
soup = BeautifulSoup(page.content, 'html.parser')

#return and display the results perhaps also displaying the 2nd 3rd cheapest ect.
#for item in List
##save the shopping list to a folder then rename it with todays date and move it somewhere else.

title = soup.find_all("h3")
title.pop() 
item_price = {}

#qtyMatched = False
matchesFound = 0
for ti in title:
    matchCount = 0
    product = ti.get_text()
    price =  ti.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.next_element.get_text()
    #if there's no price the product is probably out of stock
    if(price):
        #check if the name of the product is matched
        for word in List:
            if(word.lower() in str(product.lower())):
                matchCount +=1
        if(matchCount == len(List)):
                item_price.update({product:"£"+price})
                matchesFound +=1

#if the input is matched
if(matchesFound > 0):
    print("\nResults")
    print("Matched the item(s) "+', '.join(List)+" in multiples needed for a qty of "+qtyNeeded)
    print("These items are in stock according to the "+Tesco.getName()+ " website as of " + dateNow+"\n" )

    totalCosts = {}
    
    #list of sorted results  pairs (key = priceAsending[0]: value = priceAsending[1])
    priceAsending = sorted(item_price.items(), key=lambda x: x[1])

    #list of total costs
    for pp in priceAsending:
        #make a list with only the ints taken from the item_price "values"
        qtyPerItemList = [int(i) for i in pp[0].split() if i.isdigit()]
        qtyPerItem = qtyPerItemList[0]
        if(qtyNeeded):
            Qtyofitemsneeded = howMany(int(qtyNeeded),qtyPerItem)
        else:
            Qtyofitemsneeded = 1
        totalCost = str(total(Qtyofitemsneeded,pp[1]))
        totalCosts.update({pp[0]:totalCost})
        print(Tesco.getName())
        print("Name : "+ pp[0] )
        print("Qty per item : "+str(qtyPerItem))
        print("Price per item : "+pp[1])
        print("Qty of items needed : "+ str(Qtyofitemsneeded))
        print("Total cost : £"+ totalCost)

    #cheapest
    #need to look this up properly
    key_min = min(totalCosts.keys(), key=(lambda k: totalCosts[k]))
    cheapest_item = {key_min:"£"+totalCosts[key_min]}
    print("\n")
    print("This is the cheapest item!")
    print("\n")
    for cp in cheapest_item:
        qtyPerItemList = [int(i) for i in cp.split() if i.isdigit()]
        qtyPerItem =qtyPerItemList[0]
        if(qtyNeeded):
            Qtyofitemsneeded = howMany(int(qtyNeeded),qtyPerItem)
        else:
            Qtyofitemsneeded = 1
        print(Tesco.getName())
        print("Name : "+ cp )
        print("Qty per item : "+str(qtyPerItem))
        print("Price per item : "+item_price[cp])
        print("Qty of items needed : "+ str(Qtyofitemsneeded))
        print("Total cost : "+ cheapest_item[cp])
        #if the input is not matched
else:
    print("No matches found!")
    for nm in List:
        if(nm.lower() in product.lower()):
            print(nm+ " was found ")
        else:
            print(nm+ " was not found ")
                    